#
# Path to the wine directory or the wine executable. When you
# specify a directory it should contain /bin/wine.
#
winePath            = $share/wine

#
# Path to the wine prefix containing Adobe Reader
#
winePrefix          = $HOME/.wine-pipelight

#
# The wine architecture for the wine prefix containing Adobe Reader
#
wineArch            = win32

#
# Path to the plugin loader executable
# (Should be set correctly by the make script)
#
pluginLoaderPath    = $share/pluginloader.exe

#
# Path to the runtime DLLs (libgcc_s_sjlj-1.dll, libspp-0.dll,
# libstdc++-6.dll). Only necessary when these DLLs are not in the same
# directory as the pluginloader executable.
#
gccRuntimeDlls      = @@GCC_RUNTIME_DLLS@@

#
# Path and name to the Adobe Reader library
# You should prefer using regKey and consider this option only if you have
# multiple versions of the Adobe Reader plugin installed inside the Wineprefix.
#
# dllPath           = C:\Program Files\Adobe\Reader 11.0\Reader\AIR
# dllName           = nppdf32.dll

#
# Name of the registry key at HKCU\Software\MozillaPlugins\ or
# HKLM\Software\MozillaPlugins\ where to search for the plugin path.
#
# You should use this option instead of dllPath/dllName in most cases
# since you do not need to alter dllPath on a Adobe Reader update.
#
regKey              = Adobe Reader

#
# Adobe Reader does not offer any version number to the browser
#
# fakeVersion       = Adobe PDF Plug-In For Firefox and Netscape 11.0.04

#
# overwriteArg allows to overwrite/add initialization arguments
# passed by websites to Adobe Reader applications. Since most
# of the time PDF readers are started by files with PDF extensions
# opened in the browser and not via an embed tag, it's unlikely
# that this is ever used.
#
# overwriteArg =
#

#
# windowlessmode refers to a term of the Netscape Plugin API and
# defines a different mode of drawing and handling events.
# [default: false]
#
windowlessMode      = false

#
# Path to the dependency installer script provided by the compholio
# package. (optional)
#
dependencyInstaller = $share/install-dependency

#
# Dependencies which should be installed for this plugin via the
# dependencyInstaller, can be used multiple times. (optional)
#
# Useful values for Adobe Reader are:
#   wine-mspatcha-installer
#   wine-adobereader-installer
#
dependency          = wine-mspatcha-installer
dependency          = wine-adobereader-installer

#
# Doesn't show any dialogs which require manual confirmation during
# the installation process, like EULA or DRM dialogs.
# [default: true]
#
quietInstallation   = @@QUIET_INSTALLATION@@
